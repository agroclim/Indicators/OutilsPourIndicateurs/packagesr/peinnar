#' Compute Phenological Stages for Wheat
#' @description
#' This function computes the phenological stages of wheat based on 
#' temperature data provided in a RasterStack format. It analyzes the 
#' temperature data over the specified growing season and returns the 
#' stages of development for each year.
#'
#' @param TmoyRast RasterStack containing daily average temperature data.
#' @param EPSG Character string specifying the coordinate reference system (default is "epsg:27572").
#' @param TDMIN Minimum temperature threshold for development (default is 0.0).
#' @param TDMAX Maximum temperature threshold for development (default is 28.0).
#' @param TCXSTOP Temperature threshold for stopping the growth (default is 35.0).
#' @param SENSIPHOT Sensitivity to photoperiod (default is 0).
#' @param PHOBASE Base photoperiod (default is 6.3).
#' @param PHOSTAT Photoperiod status (default is 20).
#' @param TFROID Frost threshold (default is 6.5).
#' @param AMPFROID Frost amplitude (default is 10).
#' @param JVCMINI Minimum value for JVC (default is 7).
#' @param jvc JVC parameter (default is 35).
#' @param BBCH Numeric vector specifying the BBCH growth stages (default is c(120, 210, 260, 245, 700)).
#' @param ComputeMidResult Logical indicating whether to compute intermediate results (default is True).
#' @param PathToMidResult Path to store intermediate results (default is NULL).
#' @return A list containing the phenological stages for each year, including:
#' \itemize{
#'   \item S0: Sowing.
#'   \item S1 : Emergence
#'   \item S3: Maximum leaf surface area
#'   \item S4: Grain filling.
#'   \item S4S5: Maturity
#' }
#' @references
#' Brisson, N., Gate, P., Gouache, D., Charmet, G., & Buis, S. (2009). 
#' STICS: a generic model for the simulation of crops and their interactions 
#' with the environment. *Agricultural Systems*, 101(3), 109-121.
#' @export

ComputePhenologicalStagesWheat<-function(TmoyRast,EPSG="epsg:27572",TDMIN = 0.0,TDMAX   = 28.0,
                                    TCXSTOP = 35.0,SENSIPHOT = 0,PHOBASE   = 6.3,
                                    PHOSTAT   = 20,  TFROID    = 6.5,AMPFROID  = 10,JVCMINI   = 7, jvc=35,BBCH=c(120,210,260,245,700),
                                    ComputeMidResult=F,PathToMidResult=NULL){

TmoyRast<-TmoyRast
terra::crs(TmoyRast)<-EPSG
Times<-terra::time(TmoyRast)

if(ComputeMidResult==T){
  peinnar::ComputeIntermediateResult(RasterStack = TmoyRast,pathToWriteMidResult =PathToMidResult,TFROID =TFROID,AMPFROID =  AMPFROID)
}
photoperiode<-terra::rast(paste0(PathToMidResult,"/photoperiode.tif"))
photoperiodeBis<-terra::rast(paste0(PathToMidResult,"/photoperiodeBis.tif"))
JVI<-terra::rast(paste0(PathToMidResult,"/JVIRast.tif"))

YearOfNc<-lubridate::year(Times)
UniqueYear<-unique(YearOfNc)
S0S1Final<-list()
S1S2Final<-list()
S2S3Final<-list()
S3S4Final<-list()
S4S5Final<-list()

###Boucle sur les années#
for (year in 2:length(UniqueYear)){

  T0<-Sys.time()
  cat("traitement de l'annee : ",UniqueYear[year],"\n")
  YearOfHarvest<-UniqueYear[year]
  ########Selection de lannée d'interet et de celle d'avant (culture sur deux ans)##"

  YearOfHarvestDays<-which(YearOfNc==YearOfHarvest)
  TmoyRastYearOfHarvest<-TmoyRast[[YearOfHarvestDays]]
  PreviousYear<-YearOfHarvest-1

  YearPreviousDays<-which(YearOfNc==PreviousYear)
  TmoyRastYearPreviousYear<-TmoyRast[[YearPreviousDays]]

  TmoyRastYear<-c(TmoyRastYearPreviousYear,TmoyRastYearOfHarvest)

  PosYears<-which(YearOfNc%in%c(YearOfHarvest,YearOfHarvest-1))
  JVIYear<-JVI[[PosYears]]
  DOY720<-1:terra::nlyr(TmoyRastYear)
  sowing_dates<-288
  if(terra::nlyr(TmoyRastYearPreviousYear)==366){
    sowing_dates<-sowing_dates+1
  }

  ####On prépare les données pour l'année en cours
  UDEVECULT_RAST<-terra::app(TmoyRastYear,UDEVCULT_Rast,TDMIN=TDMIN,TDMAX=TDMAX,TCXSTOP=TCXSTOP)

  if(terra::nlyr(TmoyRastYearOfHarvest)==366){
    photoPeriodeYear<-c(photoperiode,photoperiodeBis)
  }
  if(terra::nlyr(TmoyRastYearPreviousYear)==366){
    photoPeriodeYear<-c(photoperiodeBis,photoperiode)
  }
  if(terra::nlyr(TmoyRastYearPreviousYear)==365 & terra::nlyr(TmoyRastYearOfHarvest)==365){
    photoPeriodeYear<-c(photoperiode,photoperiode)
  }

  DatesSow200<-sowing_dates:terra::nlyr(TmoyRastYear)
  # TmoyRastYears0s5<-TmoyRastYear[[DatesSow200]]
  # TmoyRastYears0s5Mat<-as.matrix(TmoyRastYears0s5)
  #
  SENSIPHOT_temp=SENSIPHOT
  PHOBASE_temp=PHOBASE
  PHOSTAT_temp=PHOSTAT

  RFPI<-terra::app(photoPeriodeYear,RFPI_cal,SENSIPHOT=SENSIPHOT_temp,PHOBASE=PHOBASE_temp,PHOSTAT=PHOSTAT_temp)

  ###s0s1#####
  accum_s0s1<-terra::app(UDEVECULT_RAST[[sowing_dates:terra::nlyr(UDEVECULT_RAST)]],cumsum)
  s0s1<-terra::which.lyr(accum_s0s1>BBCH[1])
  s0s1<-s0s1+sowing_dates

  ####Calcul des jours vernalisant à partir de s0
  RFVI = terra::app(c(s0s1,JVIYear),RFVI_calRast,jvc=jvc,JVCMINI=JVCMINI)

  ######s1s2#### Vernalisation = T, Photoperiode =T
  UPVTs1s2<-UPVT_RAST(PreviousPhase = s0s1,UDEVECULT = UDEVECULT_RAST,RFPI = RFPI,RFVI = RFVI,Vernalisation = T,photoperiode = T)
  accum_s1s2<-terra::app(UPVTs1s2,cumsum)
  s1s2<-terra::which.lyr(accum_s1s2>BBCH[2])

  ######s2s3#### Vernalisation = T, Photoperiode =T
  UPVTs2s3<-UPVT_RAST(PreviousPhase = s1s2,UDEVECULT = UDEVECULT_RAST,RFPI = RFPI,RFVI = RFVI,Vernalisation = T,photoperiode = T)
  accum_s2s3<-terra::app(UPVTs2s3,cumsum)
  s2s3<-terra::which.lyr(accum_s2s3>BBCH[3])

  ######s3s4#### Vernalisation = T, Photoperiode =T
  UPVTs3s4<-UPVT_RAST(PreviousPhase = s2s3,UDEVECULT = UDEVECULT_RAST,RFPI = RFPI,RFVI = RFVI,Vernalisation = T,photoperiode = T)
  accum_s3s4<-terra::app(UPVTs3s4,cumsum)
  s3s4<-terra::which.lyr(accum_s3s4>BBCH[4])

  ######s4s5####Vernalisation =F, Photoperiode =F
  UPVTs4s5<-UPVT_RAST(PreviousPhase = s3s4,UDEVECULT = UDEVECULT_RAST,RFPI = RFPI,RFVI = NULL,Vernalisation = F,photoperiode = F)
  accum_s4s5<-terra::app(UPVTs4s5,cumsum)
  s4s5<-terra::which.lyr(accum_s4s5>BBCH[5])

  ####Chargement des resutats
  S0S1Final[[year]]<-s0s1
  S1S2Final[[year]]<-s1s2
  S2S3Final[[year]]<-s2s3
  S3S4Final[[year]]<-s3s4
  S4S5Final[[year]]<-s4s5
  T1<-Sys.time()
  cat("Done in " ,T1-T0,"\n")
}

return(Pheno=list(S0S1=S0S1Final,S1S2=S1S2Final,S2S3=S2S3Final,S3S4=S3S4Final,S4S5=S4S5Final))
}
